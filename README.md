# Manila

A very simple web router using per-route interceptor functions.

## Install

Add the following to your _composer.json_'s [_repositories_](https://getcomposer.org/doc/05-repositories.md#vcs) section:

    {
      "type": "vcs",
      "url": "https://gitlab.com/jasonmm/manila"
    }

Then run:

    composer require shadybrooksoftware/manila

