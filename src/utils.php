<?php
declare(strict_types = 1);
namespace ShadyBrookSoftware\Manila;

use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @param string $path
 *
 * @return callable
 */
function redirectToHandler(string $path) : callable {
    return function (ServerRequestInterface $request) use ($path) : ResponseInterface {
        $uri = $request->getUri();
        $newPath = $path;
        if( $uri->getQuery() ) {
            $newPath .= '?' . $uri->getQuery();
        }
        $redirectStatus = strtolower($request->getMethod()) === 'post' ? 307 : 301;

        return new RedirectResponse($newPath, $redirectStatus);
    };
}

/**
 * Adds routes to the $routes array that redirect a trailing slash version of
 * each route already in $routes to the non-trailing slash version.
 *
 * @param RouteDef[] $routes
 *
 * @return RouteDef[]
 */
function addSlashRouteRedirects(array $routes) : array {
    $trailingSlashRoutes = array_map(function (RouteDef $route) : RouteDef {
        return new RouteDef($route->method, $route->path . '/', $route->interceptors, '\ShadyBrookSoftware\Manila\redirectToHandler');
    }, $routes);

    return array_merge($routes, $trailingSlashRoutes);
}
