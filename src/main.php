<?php
declare(strict_types=1);
namespace ShadyBrookSoftware\Manila;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\ServerRequestFactory;
use Psr\Http\Message\ResponseInterface;

/**
 * Builds and returns a function composed from the route's interceptors and the
 * route handler.
 *
 * The returned function calls the 'before' interceptors in order. If all of
 * the 'before' interceptors return a Request object then the
 * $route->handler function is called followed by each 'after' interceptor in
 * order.
 *
 * @param \ShadyBrookSoftware\Manila\RouteDef $route
 *
 * @return callable
 */
function buildRouteHandler(RouteDef $route): callable {
    $beforeFns =
        array_filter(
            array_map(
                function(array $interceptor) {
                    return $interceptor['before'] ?? false;
                },
                $route->interceptors));
    $afterFns =
        array_filter(
            array_map(
                function(array $interceptor) {
                    return $interceptor['after'] ?? false;
                },
                $route->interceptors));
    $handler = $route->handler;

    return function(ServerRequest $request) use ($beforeFns, $afterFns, $handler): ResponseInterface {
        // Each 'before' interceptor is called individually because 'before'
        // interceptors can return either a Request or a Response. If a Response
        // is returned then then no more interceptors are called and it is that
        // Response that is returned to the program.
        foreach( $beforeFns as $fn ) {
            $result = $fn($request);
            if( $result instanceof ServerRequest ) {
                $request = $result;
            } else {
                return $result;
            }
        }

        // If the $handler is the name of a class and that class has the magic
        // method to make it callable then we instantiate the class so it can
        // be called.
        if( class_exists($handler) && method_exists($handler, '__invoke') ) {
            $handler = new $handler();
        }

        // The application's request handler and all 'after' interceptors are
        // called together because none of them can short circuit.
        $fns = array_merge([$handler], $afterFns);
        $handlerPlusAfterFn = call_user_func_array('\Functional\compose', $fns);

        return call_user_func($handlerPlusAfterFn, $request);
    };
}

/**
 * Returns the FastRoute dispatcher for the given $routes.
 *
 * @param RouteDef[] $routes
 *
 * @return \FastRoute\Dispatcher
 */
function buildDispatcher(array $routes): Dispatcher {
    return \FastRoute\simpleDispatcher(function(RouteCollector $r) use ($routes): void {
        foreach( $routes as $route ) {
            $routeHandlerChain = buildRouteHandler($route);
            $r->addRoute($route->method, $route->path, $routeHandlerChain);
        }
    });
}

/**
 * Outputs the $response to the browser.
 *
 * @param ResponseInterface $response
 */
function outputResponse(ResponseInterface $response): void {
    if( headers_sent() ) {
        throw new \RuntimeException('Headers were already sent.');
    }

    $statusLine = sprintf('HTTP/%s %s %s',
                          $response->getProtocolVersion(),
                          $response->getStatusCode(),
                          $response->getReasonPhrase());
    header($statusLine, true);

    foreach( $response->getHeaders() as $name => $values ) {
        $headerStr = sprintf('%s: %s', $name, $response->getHeaderLine($name));
        header($headerStr, false);
    }

    echo $response->getBody();
}

/**
 * Given an array of route definitions this function calls the correct handler
 * (and interceptors) and returns a response object.
 *
 * @param RouteDef[] $routes
 * @param callable   $notFoundFn          A function that takes two arguments
 *                                        (HTTP method, URI path) and returns a
 *                                        `Psr\Http\Message\ResponseInterface`.
 *                                        This function is called only when the
 *                                        requested route is not found
 *                                        (i.e. a 404).
 * @param callable   $methodNotAllowedFn  A function that takes two arguments
 *                                        (HTTP method, URI path) and returns a
 *                                        `Psr\Http\Message\ResponseInterface`.
 *                                        This function is only called when the
 *                                        requested route was requested with an
 *                                        invalid method (i.e. a 405).
 *
 * @return \Psr\Http\Message\ResponseInterface
 */
function handleRequest(array $routes, callable $notFoundFn, callable $methodNotAllowedFn): ResponseInterface {
    $request = ServerRequestFactory::fromGlobals();

    $method = $request->getMethod();
    $uriPath = $request->getUri()->getPath();

    $dispatcher = buildDispatcher($routes);
    $routeInfo = $dispatcher->dispatch($method, $uriPath);

    switch( $routeInfo[0] ) {
        case $dispatcher::NOT_FOUND:
            /** @var ResponseInterface $response */
            $response = $notFoundFn($method, $uriPath);
            $response->withStatus(404);
            break;
        case $dispatcher::METHOD_NOT_ALLOWED:
            /** @var ResponseInterface $response */
            $response = $methodNotAllowedFn($method, $uriPath);
            $response->withStatus(404);
            break;
        case $dispatcher::FOUND:
        default:
            foreach( $routeInfo[2] as $name => $value ) {
                $request = $request->withAttribute($name, $value);
            }
            /** @var ResponseInterface $response */
            $response = $routeInfo[1]($request);
            break;
    }

    return $response;
}

/**
 * Given an array of route definitions this function calls the correct handler
 * (and interceptors) and outputs the response to the browser.
 *
 * @param RouteDef[] $routes
 * @param callable   $notFoundFn          A function that takes two arguments
 *                                        (HTTP method, URI path) and returns a
 *                                        `Psr\Http\Message\ResponseInterface`.
 *                                        This function is called only when the
 *                                        requested route is not found
 *                                        (i.e. a 404).
 * @param callable   $methodNotAllowedFn  A function that takes two arguments
 *                                        (HTTP method, URI path) and returns a
 *                                        `Psr\Http\Message\ResponseInterface`.
 *                                        This function is only called when the
 *                                        requested route was requested with an
 *                                        invalid method (i.e. a 405).
 *
 * @return bool
 */
function run(array $routes, callable $notFoundFn, callable $methodNotAllowedFn): bool {
    $response = handleRequest($routes, $notFoundFn, $methodNotAllowedFn);
    outputResponse($response);

    return true;
}
