<?php
declare(strict_types=1);
namespace ShadyBrookSoftware\Manila;

/**
 * Class RouteDef holds data for the parts of a route definition.
 */
class RouteDef {
    /**
     * @var string
     */
    public $method = 'GET';

    /**
     * @var string
     */
    public $path = '';

    /**
     * @var array
     */
    public $interceptors = [];

    /**
     * @var callable|string
     */
    public $handler;

    /**
     * RouteDef constructor.
     *
     * @param string          $method
     * @param string          $path
     * @param array           $interceptors
     * @param callable|string $handler
     */
    public function __construct(string $method, string $path, array $interceptors, $handler) {
        $this->handler = $handler;
        $this->interceptors = $interceptors;
        $this->method = $method;
        $this->path = $path;
    }
}
